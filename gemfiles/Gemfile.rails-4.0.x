source "https://rubygems.org"

gemspec :path => ".."

gem "rails", "~> 4.0.13"
gem "minitest", "~> 4.7"
gem 'http_accept_language'